//Copyright by Szymon Przyszcz


#include <iostream>
using namespace std;


// Program wyswietla na ekranie tekst "Witaj WIOSNO"

/// @brief Główna funkcja programu, wyświetla fajny komunikat...
///
/// @return zawsze zwraca wartość 0.

int main()
{
	cout << "Witaj WIOSNO"<<endl;
	
	return 0;
}